# JobVacancy Acceptance Tests


## Run test in host 

Check chrome and chrome driver installation and version.

```bash
# start the harness
docker-compose up

# run test 
./run_tests_in_host.sh
```

## Run test inside the compose harness

```bash
docker-compose -f docker-compose.yml -f docker-compose.testing.yml up --abort-on-container-exit --exit-code-from testrunner
```
