require 'selenium/webdriver'

TARGET_HOST=ENV['TARGET_HOST'] || 'http://job-vacancy-staging.herokuapp.com'
CHROME_DRIVER_PATH=ENV['CHROME_DRIVER_PATH'] || '/usr/bin/chromedriver'
SHOULD_USE_FULL_BROWSER = ENV['SHOULD_USE_FULL_BROWSER'] 
options = Selenium::WebDriver::Chrome::Options.new(args: ['--no-default-browser-check', '--headless', '--no-sandbox', '--disable-dev-shm-usage'])
if SHOULD_USE_FULL_BROWSER
  options = Selenium::WebDriver::Chrome::Options.new(args: ['--no-default-browser-check', '--disable-dev-shm-usage'])
end
service = Selenium::WebDriver::Service.chrome(path:CHROME_DRIVER_PATH)
driver = Selenium::WebDriver.for :chrome, service: service, options: options

def url_for(route)
  "#{TARGET_HOST}#{route}"
end

Before do |scenario|
  @driver = driver
  @driver.get(url_for '/health/reset')
end
