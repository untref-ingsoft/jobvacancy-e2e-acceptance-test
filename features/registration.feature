Feature: Registration

  Scenario: Succesfull registration
    Given I am not registered
    When I pick email 'john@test.com' with password 'Passw0rd!'
    Then I am sucesfully registered

  @wip
  Scenario: Failed registration
    Given there is already a user registed as 'john@test.com'
    When I pick email 'john@test.com' with password 'Passw0rd!'
    Then my registration is rejected
