OFFER_CREATED_MESSAGE = 'Offer created'.freeze
OFFER_UPDATED_MESSAGE = 'Offer updated'.freeze
OFFER_DELETED_MESSAGE = 'Offer deleted'.freeze
REGISTRATION_MENU = 'register'.freeze
JOB_OFFERS_MENU = 'Job offers'.freeze

Given(/^I am logged in as job offerer$/) do
  @driver.get(url_for '/register')
  @driver.find_element(id: 'user_name').send_keys 'offerer@test.com'
  @driver.find_element(id: 'user_email').send_keys 'offerer@test.com'
  @driver.find_element(id: 'user_password').send_keys 'Passw0rd!'
  @driver.find_element(id: 'user_password_confirmation').send_keys 'Passw0rd!'
  @driver.find_element(css: '[value="Create"]').click

  @driver.get(url_for '/login')
  @driver.find_element(id: 'user_email').send_keys 'offerer@test.com'
  @driver.find_element(id: 'user_password').send_keys 'Passw0rd!'
  @driver.find_element(id: 'loginButton').click
  /offerer@test.com/.match(@driver.page_source)
end

When(/^I create a new offer with "(.*?)" as the title$/) do |title|
  @driver.get(url_for '/job_offers/new')
  @driver.find_element(:id, 'job_offer_form_title').send_keys title
  @driver.find_element(:id, 'createButton').click
end

Then(/^I should see a offer created confirmation message$/) do
  expect(@driver.page_source).to include OFFER_CREATED_MESSAGE
end

Then(/^I should see a offer updated confirmation message$/) do
  page.should have_content(OFFER_UPDATED_MESSAGE)
end

Then(/^I should see a offer deleted confirmation message$/) do
  page.should have_content(OFFER_DELETED_MESSAGE)
end

Then(/^I should see "(.*?)" in my offers list$/) do |content|
  @driver.get(url_for '/job_offers/my')
  expect(@driver.page_source).to include content
end

Then(/^I should not see "(.*?)" in my offers list$/) do |content|
  visit '/job_offers/my'
  page.should_not have_content(content)
end

Then(/^I should see a registration menu$/) do
  page.should have_content(REGISTRATION_MENU)
end

Then(/^I should see a job offers menu$/) do
  page.should have_content(JOB_OFFERS_MENU)
end

Given(/^I have "(.*?)" offer in my offers list$/) do |offer_title|
  JobOfferRepository.new.delete_all

  visit '/job_offers/new'
  fill_in('job_offer_form[title]', with: offer_title)
  click_button('Create')
end

When(/^I change the title to "(.*?)"$/) do |new_title|
  click_link('Edit')
  fill_in('job_offer_form[title]', with: new_title)
  click_button('Save')
end

And(/^I delete it$/) do
  click_button('Delete')
end
