require 'byebug'

Given('I am not registered') do
  # nothing to do here
end

When('I pick email {string} with password {string}') do |email, password|
  @driver.get(url_for '/register')
  @driver.find_element(id: 'user_name').send_keys email
  @driver.find_element(id: 'user_email').send_keys email
  @driver.find_element(id: 'user_password').send_keys password
  @driver.find_element(id: 'user_password_confirmation').send_keys password
  @driver.find_element(css: '[value="Create"]').click
end

Then('I am sucesfully registered') do
  message = @driver.find_element(id: 'flashMessage')
  expect(message.text).to include 'User created'
end

Given('there is already a user registed as {string}') do |email|
  password = 'Password!'
  @driver.get(url_for '/register')
  @driver.find_element(id: 'user_name').send_keys email
  @driver.find_element(id: 'user_email').send_keys email
  @driver.find_element(id: 'user_password').send_keys password
  @driver.find_element(id: 'user_password_confirmation').send_keys password
  @driver.find_element(css: '[value="Create"]').click  
end

Then('my registration is rejected') do
  pending # Write code here that turns the phrase above into concrete actions
end
