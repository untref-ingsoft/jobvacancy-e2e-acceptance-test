#!/bin/bash

export TARGET_HOST=http://localhost:3000
export CHROME_DRIVER_PATH=/Users/nicopaez/chromedriver
export SHOULD_USE_FULL_BROWSER=true
bundle install
bundle exec rake
